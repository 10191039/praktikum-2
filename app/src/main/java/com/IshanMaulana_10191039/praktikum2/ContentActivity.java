package com.IshanMaulana_10191039.praktikum2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

public class ContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        TabLayout tablayout=findViewById(R.id.TLcontent);
        ViewPager viewpager=findViewById(R.id.VPcontent);
        tablayout.addTab(tablayout.newTab().setText("home"));
        tablayout.addTab(tablayout.newTab().setText("status"));
        tablayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        FragmentAdapter adapter=new FragmentAdapter(this,tablayout.getTabCount(),getSupportFragmentManager());
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}